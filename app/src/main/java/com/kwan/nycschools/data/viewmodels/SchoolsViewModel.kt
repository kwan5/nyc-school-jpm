package com.kwan.nycschools.data.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.kwan.nycschools.common.Resource
import com.kwan.nycschools.domain.data.SATScoresData
import com.kwan.nycschools.domain.data.SchoolData
import com.kwan.nycschools.domain.repos.SchoolsRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SchoolsViewModel @Inject constructor(
    private val repo: SchoolsRepo
): ViewModel() {

    val schools : LiveData<Resource<List<SchoolData>>> = repo.schools
    val satScores : LiveData<Resource<List<SATScoresData>>> = repo.satScores

    fun refreshSchools() {
        repo.fetchSchools()
    }

    fun getSATScores(dbn: String) {
        repo.getSatScores(dbn)
    }
}