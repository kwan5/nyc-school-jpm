package com.kwan.nycschools.data.repos

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.kwan.nycschools.common.Resource
import com.kwan.nycschools.domain.apiservices.NYCOpenDataService
import com.kwan.nycschools.domain.data.SATScoresData
import com.kwan.nycschools.domain.data.SchoolData
import com.kwan.nycschools.domain.repos.SchoolsRepo
import retrofit2.Call
import retrofit2.Response
import javax.inject.Inject

class DefaultSchoolsRepo @Inject constructor(
    private val apiService: NYCOpenDataService
) : SchoolsRepo {
    private var schoolsCache: List<SchoolData>? = null
    private var satScoresCache = mutableMapOf<String, List<SATScoresData>>()

    private val _schools = MutableLiveData<Resource<List<SchoolData>>>()
    override val schools: LiveData<Resource<List<SchoolData>>>
        get() = _schools

    private val _satScores = MutableLiveData<Resource<List<SATScoresData>>>()
    override val satScores: LiveData<Resource<List<SATScoresData>>>
        get() = _satScores

    override fun fetchSchools() {
        if (schoolsCache != null) {
            _schools.value = Resource.Success(schoolsCache!!)
        } else {
            apiService.getHighSchools().enqueue(object: retrofit2.Callback<List<SchoolData>> {
                override fun onResponse(
                    call: Call<List<SchoolData>>,
                    response: Response<List<SchoolData>>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        schoolsCache = response.body()!!.filterNot { it.dbn == null }
                        _schools.value = Resource.Success(schoolsCache!!)
                    } else {
                        _schools.value = Resource.Error("Error retrieving Schools")
                        // LOG to firebase
                    }
                }

                override fun onFailure(call: Call<List<SchoolData>>, t: Throwable) {
                    _schools.value = Resource.Error("Error retrieving schools")
                    // LOG throwable to firebase
                }
            })
        }
    }

    override fun getSatScores(dbn: String) {
        if (satScoresCache[dbn] != null) {
            _satScores.value = Resource.Success(satScoresCache[dbn]!!)
        } else {
            apiService.get2012SATResults(dbn).enqueue(object: retrofit2.Callback<List<SATScoresData>>{
                override fun onResponse(
                    call: Call<List<SATScoresData>>,
                    response: Response<List<SATScoresData>>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        satScoresCache[dbn] = response.body()!!
                        _satScores.value = Resource.Success(satScoresCache[dbn]!!)
                    } else {
                        _satScores.value = Resource.Error("Error retrieving SAT Scores")
                        // LOG to firebase
                    }
                }

                override fun onFailure(call: Call<List<SATScoresData>>, t: Throwable) {
                    _satScores.value = Resource.Error("Error retrieving SAT Scores")
                    // LOG throwable to firebase
                }
            })
        }
    }
}