package com.kwan.nycschools.di

import com.kwan.nycschools.data.repos.DefaultSchoolsRepo
import com.kwan.nycschools.domain.repos.SchoolsRepo
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class SchoolsAppModule {
    @Binds
    abstract fun bindSchoolsRepository(defaultSchoolsRepository: DefaultSchoolsRepo): SchoolsRepo
}