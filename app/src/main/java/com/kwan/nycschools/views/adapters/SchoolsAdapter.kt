package com.kwan.nycschools.views.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.kwan.nycschools.databinding.ItemSchoolBinding
import com.kwan.nycschools.domain.data.SchoolData

class SchoolsAdapter(
    private val onItemSelectedHandler: (SchoolData) -> Unit
) : ListAdapter<SchoolData, SchoolsAdapter.SchoolViewHolder>(diffUtilItemCallback) {

    companion object {
        val diffUtilItemCallback = object : DiffUtil.ItemCallback<SchoolData>() {
            override fun areItemsTheSame(oldItem: SchoolData, newItem: SchoolData): Boolean =
                oldItem.dbn == newItem.dbn

            override fun areContentsTheSame(oldItem: SchoolData, newItem: SchoolData): Boolean =
                oldItem.dbn == newItem.dbn
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolViewHolder {
        val binding = ItemSchoolBinding.inflate(LayoutInflater.from(parent.context))
        return SchoolViewHolder(binding, onItemSelectedHandler)
    }

    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) {
        holder.bindSchool(getItem(position))
    }

    class SchoolViewHolder(
        private val binding: ItemSchoolBinding,
        private val onItemSelectedHandler: (SchoolData) -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {

        private lateinit var school: SchoolData

        fun bindSchool(school: SchoolData) {
            this.school = school
            binding.schoolLabel.text = school.schoolName
            binding.root.setOnClickListener {
                onItemSelectedHandler(school)
            }
        }
    }
}