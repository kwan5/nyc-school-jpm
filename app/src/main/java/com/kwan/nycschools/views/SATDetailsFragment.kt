package com.kwan.nycschools.views

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.kwan.nycschools.databinding.FragmentSatDetailsBinding

class SATDetailsFragment : Fragment() {
    private lateinit var binding: FragmentSatDetailsBinding

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentSatDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val args = SATDetailsFragmentArgs.fromBundle(requireArguments())
        val score = args.satScores

        binding.schoolNameLabel.text = score.schoolName
        binding.avgCriticalReadingScoreLabel.text = "Avg Critical Reading Score: ${score.avgCriticalReadingScore}"
        binding.avgMathScoreLabel.text = "Avg Math Score: ${score.avgMathScore}"
        binding.avgWritingScoreLabel.text = "Avg Writing Score: ${score.avgWritingScore}"
        binding.avgNumSatTakersLabel.text = "Avg Num SAT Takers: ${score.numOfSATTakers}"

        activity.let {
            if (it is AppCompatActivity) {
                it.supportActionBar?.title = score.schoolName
            }
        }
    }
}