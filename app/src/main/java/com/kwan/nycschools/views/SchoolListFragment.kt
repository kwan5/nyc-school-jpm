package com.kwan.nycschools.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.kwan.nycschools.common.Resource
import com.kwan.nycschools.data.viewmodels.SchoolsViewModel
import com.kwan.nycschools.databinding.FragmentSchoolListBinding
import com.kwan.nycschools.domain.data.SchoolData
import com.kwan.nycschools.extensions.showAlert
import com.kwan.nycschools.views.adapters.SchoolsAdapter

class SchoolListFragment : Fragment() {
    private lateinit var binding: FragmentSchoolListBinding
    private val adapter = SchoolsAdapter{ showSATScores(it) }
    private val model: SchoolsViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model.schools.observe(this) {
            when (it) {
                is Resource.Success -> {
                    if (it.data.isNullOrEmpty()) {
                        showAlert("No High Schools Result, Please Try Again Later")
                    } else {
                        adapter.submitList(it.data)
                    }
                }
                is Resource.Error -> {
                    showAlert(it.message ?: "Failed to retrieve high schools")
                }
            }
        }

        model.satScores.observe(this) {
            when(it) {
                is Resource.Success -> {
                    if (it.data.isNullOrEmpty()) {
                        showAlert("No SAT Results")
                    } else {
                        // if there are multiple returns use the first on only
                        val score = it.data.firstOrNull()
                        if (score == null) {
                            showAlert("No SAT Results")
                        } else {
                            val action = SchoolListFragmentDirections.actionSchoolListFragmentToSatDetailsFragment(score)
                            findNavController().navigate(action)
                        }
                    }
                }
                is Resource.Error -> {
                    showAlert("NO SAT Results")
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentSchoolListBinding.inflate(inflater, container, false)

        binding.schoolList.adapter = adapter

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        model.refreshSchools()
    }

    private fun showSATScores(school: SchoolData) {
        if (school.dbn == null) {
            showAlert("Scores are unavailable. Please try again later.")
            // LOG to firebase, should have been filtered out during retrieval.
        } else {
            model.getSATScores(school.dbn)
        }
    }
}