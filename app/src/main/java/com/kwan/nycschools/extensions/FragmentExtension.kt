package com.kwan.nycschools.extensions

import androidx.fragment.app.Fragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder

fun Fragment.showAlert(message: String) {
    MaterialAlertDialogBuilder(requireContext())
        .setMessage(message)
        .setPositiveButton("Ok") { dialog, _ ->
            dialog.dismiss()
        }
        .show()
}