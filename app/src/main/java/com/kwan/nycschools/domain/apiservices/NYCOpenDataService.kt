package com.kwan.nycschools.domain.apiservices

import com.kwan.nycschools.domain.data.SATScoresData
import com.kwan.nycschools.domain.data.SchoolData
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface NYCOpenDataService {
    @GET("/resource/s3k6-pzi2.json")
    fun getHighSchools() : Call<List<SchoolData>>

    @GET("/resource/f9bf-2cp4.json")
    fun get2012SATResults(@Query("dbn") dbn:String): Call<List<SATScoresData>>
}