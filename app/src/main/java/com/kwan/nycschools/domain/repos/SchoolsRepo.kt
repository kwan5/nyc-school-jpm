package com.kwan.nycschools.domain.repos

import androidx.lifecycle.LiveData
import com.kwan.nycschools.common.Resource
import com.kwan.nycschools.domain.data.SATScoresData
import com.kwan.nycschools.domain.data.SchoolData

interface SchoolsRepo {
    val schools: LiveData<Resource<List<SchoolData>>>
    val satScores: LiveData<Resource<List<SATScoresData>>>

    fun fetchSchools()
    fun getSatScores(dbn: String)
}