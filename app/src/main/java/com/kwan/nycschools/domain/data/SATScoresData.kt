package com.kwan.nycschools.domain.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class SATScoresData(
    val dbn: String,
    @SerializedName("school_name")
    val schoolName: String,
    @SerializedName("num_of_sat_test_takers")
    val numOfSATTakers: String,
    @SerializedName("sat_critical_reading_avg_score")
    val avgCriticalReadingScore: String,
    @SerializedName("sat_math_avg_score")
    val avgMathScore: String,
    @SerializedName("sat_writing_avg_score")
    val avgWritingScore: String
): Parcelable