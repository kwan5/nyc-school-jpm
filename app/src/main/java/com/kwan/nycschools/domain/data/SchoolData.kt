package com.kwan.nycschools.domain.data

import com.google.gson.annotations.SerializedName

data class SchoolData(
    val dbn: String?,
    @SerializedName("school_name")
    val schoolName: String?)