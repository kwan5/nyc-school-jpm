package com.kwan.nycschools.views

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.kwan.nycschools.R
import com.kwan.nycschools.utils.RecyclerViewMatcher
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
@HiltAndroidTest
class MainActivityTest {
    @get:Rule var hiltRule = HiltAndroidRule(this)
    @get:Rule var activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Before
    fun init() {
        hiltRule.inject()
    }

    @Test
    fun makeSureTitleIsCorrectAtPosition0() {
        onView(withRecyclerView(R.id.school_list).atPosition(0))
            .check(
                matches(
                    hasDescendant(withText("School1"))
                )
            )
    }

    @Test
    fun makeSureWeCanNavigateToTheSchoolDetailsAtPosition0() {
        onView(withId(R.id.school_list))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click())
            )

        onView(withId(R.id.school_name_label))
            .check(matches(withText("School1")))
    }

    @Test
    fun makeSureWeGetAlertedWhenThereIsNoScoreAtPosition1() {
        onView(withId(R.id.school_list))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click())
            )

        onView(withText("NO SAT Results")).check(matches(isDisplayed()))
    }

    private fun withRecyclerView(id: Int) = RecyclerViewMatcher(id)
}