package com.kwan.nycschools.data.repos

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.kwan.nycschools.common.Resource
import com.kwan.nycschools.domain.data.SATScoresData
import com.kwan.nycschools.domain.data.SchoolData
import com.kwan.nycschools.domain.repos.SchoolsRepo
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FakeSchoolsRepo @Inject constructor() : SchoolsRepo {
    private var schoolsCache: List<SchoolData>? = listOf(
        SchoolData("dbn1", "School1"),
        SchoolData("dbn2", "School2")
    )
    private var satScoresCache = mutableMapOf(
        "dbn1" to listOf(
            SATScoresData("dbn1", "School1", "100","300","100", "100"))
    )

    private val _schools = MutableLiveData<Resource<List<SchoolData>>>()
    override val schools: LiveData<Resource<List<SchoolData>>>
        get() = _schools

    private val _satScores = MutableLiveData<Resource<List<SATScoresData>>>()
    override val satScores: LiveData<Resource<List<SATScoresData>>>
        get() = _satScores

    override fun fetchSchools() {
        _schools.value = Resource.Success(schoolsCache!!)
    }

    override fun getSatScores(dbn: String) {
        if (satScoresCache[dbn] != null) {
            _satScores.value = Resource.Success(satScoresCache[dbn]!!)
        } else {
            _satScores.value = Resource.Error("SCORES NOT FOUND MESSAGE")
        }
    }
}