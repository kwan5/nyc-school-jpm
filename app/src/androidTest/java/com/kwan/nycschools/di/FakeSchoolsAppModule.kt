package com.kwan.nycschools.di

import com.kwan.nycschools.data.repos.FakeSchoolsRepo
import com.kwan.nycschools.domain.repos.SchoolsRepo
import dagger.Binds
import dagger.Module
import dagger.hilt.components.SingletonComponent
import dagger.hilt.testing.TestInstallIn

@Module
@TestInstallIn(
    components = [SingletonComponent::class],
    replaces = [SchoolsAppModule::class]
)
abstract class FakeSchoolsAppModule {
    @Binds
    abstract fun bindSchoolsRepo(defaultSchoolsRepo: FakeSchoolsRepo): SchoolsRepo
}